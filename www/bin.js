const express = require('express');
const path = require('path');
const app = express();

// viewed at http://localhost:8080
app.use(express.static(path.join(__dirname, './../dist')));

app.listen(process.env.PORT || 8080)
