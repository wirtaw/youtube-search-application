import * as types from './../types';

import axios from 'axios';

const URL = `http://localhost:8081`

const state = {
  list: [],
  video: null,
  errors: []
};

const getters = {
  [types.VIDEO]: state => {
    return state.video;
  },
  [types.LIST]: state => {
    return state.list;
  }
};

const mutations = {
  [types.MUTATE_VIDEO_ITEM]: (state, payload) => {
    state.video = payload;
  },
  [types.MUTATE_VIDEO_ADD_LIST]: async (state, payload) => {
    let obj = {...payload};
    let number = 1;
    for (let item of state.list) {
      if (item.number > number) {
        number = item.number;
      }
    }
    obj.number = number + 1;
    state.list.push(obj);
    try {
      const result = await axios.post(`${URL}/add_video`, obj);
      if (result.status !== 200) {
        state.errors.push({error: result});
      }
    } catch (exc) {
      state.errors.push({error: exc});
    }
    
  },
  [types.MUTATE_VIDEO_DELETE_LIST_ITEM]: (state, payload) => {
    state.list = state.list.filter((item) => {
      return item.id.videoId !== payload
    });
  },
  [types.MUTATE_VIDEO_LOAD_LIST]: (state, payload) => {
    state.list = payload
  }
};

const actions = {
  [types.VIDEO_ITEM_SET]: ({ commit }, payload) => {
    commit(types.MUTATE_VIDEO_ITEM, payload);
    commit(types.MUTATE_VIDEO_ADD_LIST, payload);
  },
  [types.VIDEO_ITEM_DELETE]: async ({ commit }, payload) => {
    try {
      const result = await axios.delete(`${URL}/videos/${payload}`);
      if (result.status !== 200) {
        state.errors.push({error: result});
      } else {
        commit(types.MUTATE_VIDEO_DELETE_LIST_ITEM, payload);
      }
    } catch (exc) {
      state.errors.push({error: exc});
    }
  },
  [types.VIDEO_GET_LIST]: async ({ commit }) => {
    try {
      const result = await axios.get(`${URL}/videos`);
      if (result.status !== 200) {
        state.errors.push({error: result});
      } else {
        if (result.data && result.data.videos) {
          if (result.data.videos[0]) {
            commit(types.MUTATE_VIDEO_ITEM, result.data.videos[0]);
          }
          commit(types.MUTATE_VIDEO_LOAD_LIST, result.data.videos);
        }
      }
    } catch (exc) {
      state.errors.push({error: exc});
    }
  },
  [types.VIDEO_PLAY_ITEM]: ({ commit }, payload) => {
    commit(types.MUTATE_VIDEO_ITEM, payload);
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
