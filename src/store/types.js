//Getters
export const VIDEO = 'playlist/VIDEO';
export const LIST = 'playlist/LIST';

// Mutations
export const MUTATE_VIDEO_ITEM = 'playlist/MUTATE_VIDEO_ITEM';
export const MUTATE_VIDEO_ADD_LIST = 'playlist/MUTATE_VIDEO_ADD_LIST';
export const MUTATE_VIDEO_DELETE_LIST_ITEM = 'playlist/MUTATE_VIDEO_DELETE_LIST_ITEM';
export const MUTATE_VIDEO_LOAD_LIST = 'playlist/MUTATE_VIDEO_LOAD_LIST';

// Actions
export const VIDEO_ITEM_SET = 'playlist/VIDEO_ITEM_SET';
export const VIDEO_ITEM_DELETE = 'playlist/VIDEO_ITEM_DELETE';
export const VIDEO_GET_LIST = 'playlist/VIDEO_GET_LIST';
export const VIDEO_PLAY_ITEM = 'playlist/VIDEO_PLAY_ITEM';
