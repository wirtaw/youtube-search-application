import Vue from 'vue'
import App from './App.vue'
import { DateTime } from 'luxon';
import VueYouTubeEmbed from 'vue-youtube-embed'
import { store } from './store.js';

Vue.config.productionTip = false


Vue.filter('formatDate', (value) => {
  if (!value) return ''
  return DateTime.fromISO(value).toLocaleString(DateTime.DATETIME_FULL)
})

Vue.filter('getTitle', (item) => {
  if (!item || !item.snippet.title) return ''
  return item.snippet.title
})


Vue.use(VueYouTubeEmbed, { global: true, componentId: "youtube-media" })


new Vue({
  el: '#app',
  store,
  render: h => h(App)
});
