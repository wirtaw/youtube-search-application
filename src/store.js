import Vue from 'vue';
import Vuex from 'vuex';
import playlist from './store/modules/playlist.js';


Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {
    playlist
  }
});
