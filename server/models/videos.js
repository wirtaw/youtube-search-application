const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const VideoSchema = new Schema({
  number: Number,
  kind: String,
  etag: String,
  publicId: {
    kind: String,
    videoId: String
  },
  snippet: {
    publishedAt: Date,
    channelId: String,
    title: String,
    description: String,
    thumbnails: {
      default: {
        url: String,
        width: Number,
        height: Number
      },
      medium: {
        url: String,
        width: Number,
        height: Number
      },
      high: {
        url: String,
        width: Number,
        height: Number
      }
    },
    channelTitle: String,
    liveBroadcastContent: String
  }
});

const Video = mongoose.model("Video", VideoSchema);
module.exports = Video;
