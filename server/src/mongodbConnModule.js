let mongoose = require('mongoose');

const connectionStr = 'mongodb://youtubeviewer:qWs4H_S0TLqWMweZeKwXDuF@ds147926.mlab.com:47926/youtube-videos';
let mongooseOptions = {
  autoIndex: false, // Don't build indexes
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 10, // Maintain up to 10 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  useNewUrlParser: true,
  bufferCommands: false
};

module.exports.connect = () => {
  mongoose.connect(connectionStr, mongooseOptions);
  let db = mongoose.connection;
  db.on('error', console.error.bind(console, 'connection error'));
  db.once('open', () => {
    console.log('Connection Succeeded');
  });
  return db;
}
