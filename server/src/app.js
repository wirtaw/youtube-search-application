const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');

const app = express()
app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(cors())

const mongodb_conn_module = require('./mongodbConnModule');
const db = mongodb_conn_module.connect();

const Video = require("../models/videos");

app.get('/videos', (req, res) => {
  Video.find({}, 'id number etag kind snippet publicId', (error, videos) => {
    if (!error) {
      res.send({
        videos: videos.map((item) => {
          let video = {};
          for (let key in item._doc) {
            if (item._doc.hasOwnProperty(key)) {
              if (key !== 'publicId') {
                video[key] = item._doc[key]
              } else {
                video['id'] = item._doc[key]
              }
            }
          }
          return video
        })
      })
    } else {
      res.send({
        videos: []
      })
    }
  }).sort({ _id: -1 })
})

app.post('/add_video', (req, res) => {
  let obj = {...req.body};
  let video = {};
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      if (key !== 'id') {
        video[key] = obj[key]
      } else {
        video['publicId'] = obj[key]
      }
    }
  }
  const newVideo = new Video(video);
  
  newVideo.save(function (error) {
    res.send({
      success: !error
    })
  })
})


app.delete('/videos/:id', (req, res) => {
  Video.remove({
    'publicId.videoId': req.params.id
  }, (error) => {
    res.send({
      success: !error
    })
    
  })
})

app.listen(process.env.PORT || 8081);
