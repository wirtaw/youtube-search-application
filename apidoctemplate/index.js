/** @api {get} /videos Get Videos
 * @apiName GetVideosList
 * @apiGroup Videos
 *
 * @apiSuccess {Array} videos List of the Videos.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "videos": [{
 *          "id": {
 *              "kind": "youtube#video",
 *              "videoId": "_CL6n0FJZpk"
 *          },
 *          "snippet": {
 *              "thumbnails": {
 *                  "default": {
 *                      "url": "https://i.ytimg.com/vi/_CL6n0FJZpk/default.jpg",
 *                      "width": 120,
 *                      "height": 90
 *                  },
 *                  "medium": {
 *                      "url": "https://i.ytimg.com/vi/_CL6n0FJZpk/mqdefault.jpg",
 *                      "width": 320,
 *                      "height": 180
 *                  },
 *                  "high": {
 *                      "url": "https://i.ytimg.com/vi/_CL6n0FJZpk/hqdefault.jpg",
 *                      "width": 480,
 *                      "height": 360
 *                  }
 *              },
 *              "publishedAt": "2011-10-27T16:48:34.000Z",
 *              "channelId": "UCx4Lxn5CzeG99X01__YsJpw",
 *              "title": "Dr. Dre - Still D.R.E. ft. Snoop Dogg",
 *              "description": "Get COMPTON the NEW ALBUM from Dr. Dre on Apple Music: http://smarturl.it/Compton Music video by Dr. Dre performing Still D.R.E.. (C) 1999 Aftermath ...",
 *              "channelTitle": "DrDreVEVO",
 *              "liveBroadcastContent": "none"
 *          },
 *          "_id": "5cc44e9640fd962a6422d0be",
 *          "kind": "youtube#searchResult",
 *          "etag": "\"XpPGQXPnxQJhLgs6enD_n8JR4Qk/pIGzuT8I4X3uY71Gu6HCIQzbOJk\"",
 *          "number": 4
 *      },
 *      {
 *          "id": {
 *              "kind": "youtube#video",
 *              "videoId": "8QSgNM9yNjo"
 *          },
 *          "snippet": {
 *              "thumbnails": {
 *                  "default": {
 *                      "url": "https://i.ytimg.com/vi/8QSgNM9yNjo/default.jpg",
 *                      "width": 120,
 *                      "height": 90
 *                  },
 *                  "medium": {
 *                      "url": "https://i.ytimg.com/vi/8QSgNM9yNjo/mqdefault.jpg",
 *                      "width": 320,
 *                      "height": 180
 *                  },
 *                  "high": {
 *                      "url": "https://i.ytimg.com/vi/8QSgNM9yNjo/hqdefault.jpg",
 *                      "width": 480,
 *                      "height": 360
 *                  }
 *              },
 *              "publishedAt": "2010-03-24T22:01:22.000Z",
 *              "channelId": "UCRpjHHu8ivVWs73uxHlWwFA",
 *              "title": "Lena - Satellite (Germany)",
 *              "description": "Powered by http://www.eurovision.tv Lena will represent Germany with the song Satellite at the 2010 Eurovision Song Contest in Oslo (Norway), taking place on ...",
 *              "channelTitle": "Eurovision Song Contest",
 *              "liveBroadcastContent": "none"
 *          },
 *          "_id": "5cc449a99c6f962613cb3291",
 *          "kind": "youtube#searchResult",
 *          "etag": "\"XpPGQXPnxQJhLgs6enD_n8JR4Qk/v0VgPxAi9SkTHFh64mxaj6cq9cw\"",
 *          "number": 3
 *      },
 *      ...]
 *     }
 *
 */

/** @api {post} /add_video Create Video
 * @apiName NewVideo
 * @apiGroup Videos
 *
 * @apiParam {Object} video Information about Youtube video
 *
 * @apiSuccess {Boolean} success Request answer.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true
 *     }
 *
 */


/** @api {delete} /videos/:id Delete Video by ID
 * @apiName DeleteVideo
 * @apiGroup Videos
 *
 * @apiParam {String} :id Video unique ID.
 *
 * @apiSuccess {Boolean} success Request answer.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true
 *     }
 *
 */
